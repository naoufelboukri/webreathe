# Webreathe

## Initialisation du projet 

1. Création de la base de données
    - **Nom** : Webreathe
    - **Code** : utf8-general-ci
2. Télécharger les dépendances 
    > composer install 
    
    > npm install

___ 

## Projet 
  

### 1/ Base de données 

La base de données doit répertorier les modules, ces détails et l'historique de fonctionnement. 
La page de visualisation doit regrouper les informations suivantes : 
- Valeur mesurée actuelle
- Durée de fonctionnement
- Nombre de données envoyées 
- Etat de marche 
- Graphique permettant de suivre l'évolution de la valeur mesurée 

Ainsi, avec des dernières informations, j'ai décidé de réaliser ma base de données de la manière suivante. 
![image](infos/dictionnaire_de_donn%C3%A9es.png)

___ 

### 2/ Layouts 

#### a. Home

Concernant l'affichage de ma page et afin de répondre aux demandes, j'envisage de reprendre entièrement la charte graphique de la page officielle de [webreathe](https://webreathe.fr). 

Sur celle-ci je présente premièrement l'énoncé du test technique en ajoutant la possibilité de récupérer directement le fichier pdf qui m'a été remit en amont.

Dans la section qui suit, j'ajoute un bouton permettant d'initialiser la base de données avec des données par défaut que j'aurai pris soin d'écrire. Ces données peuvent être retrouvées dans le dossier fixtures.

Enfin dans la dernière section, on peut retrouver la table avec l'ensemble des modules existant.
